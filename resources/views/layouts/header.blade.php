<div class="header-area {!! (Request::is('light-sidebar/*') ? 'light-header' : '') !!}">
    <div class="row align-items-center">
        <div class="mobile-logo d_none_lg">
            <a href="{{ URL::to('index') }}"><img src="{{asset('assets/images/gadgetmart.png')}}" alt="logo"></a>
        </div>

        <!--==================================*
                 Navigation and Search
        *====================================-->
        <div class="col-md-6 d_none_sm d-flex align-items-center">
            <div class="nav-btn pull-left">
                <span></span>
                <span></span>
                <span></span>
            </div>
{{--            <div class="search-box pull-left">--}}
{{--                <form action="#">--}}
{{--                    <i class="ti-search"></i>--}}
{{--                    <input type="text" name="search" placeholder="Search..." required>--}}
{{--                </form>--}}
{{--            </div>--}}
        </div>
        <!--==================================*
                 End Navigation and Search
        *====================================-->

        <!--==================================*
                 Notification Section
        *====================================-->
        <div class="col-md-6 col-sm-12">
            <ul id="notification_section" class="notification-area pull-right">
                <li>
                    <span class="nav-btn pull-left d_none_lg">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </li>
                <li id="full-view" class="d_none_sm"><i class="feather ft-maximize"></i></li>
                <li id="full-view-exit" class="d_none_sm"><i class="feather ft-minimize"></i></li>
{{--                <li class="dropdown">--}}
{{--                    <i class="ti-bell dropdown-toggle" data-toggle="dropdown"><span></span></i>--}}
{{--                    <div class="dropdown-menu bell-notify-box notify-box">--}}
{{--                        <span class="notify-title">You have 3 new notifications <a href="#">view all</a></span>--}}
{{--                        <div class="nofity-list">--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb"><i class="ti-map-alt bg_blue"></i></div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>You added your Location</h3>--}}
{{--                                    <span>Just Now</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb"><i class="ti-bolt-alt bg_warning"></i></div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Your Subscription Expired</h3>--}}
{{--                                    <span>30 Seconds ago</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb"><i class="ti-heart bg_danger"></i></div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Some special like you</h3>--}}
{{--                                    <span>Just Now</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb"><i class="ti-comments bg_info"></i></div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>New Commetns On Post</h3>--}}
{{--                                    <span>30 Seconds ago</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb"><i class="ti-settings bg_secondary"></i></div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>You changed your Settings</h3>--}}
{{--                                    <span>Just Now</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb"><i class="ti-image bg_success"></i></div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Image Uploaded Successfully</h3>--}}
{{--                                    <span>Just Now</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="dropdown">--}}
{{--                    <i class="fa fa-envelope-o dropdown-toggle" data-toggle="dropdown"><span></span></i>--}}
{{--                    <div class="dropdown-menu notify-box nt-enveloper-box">--}}
{{--                        <span class="notify-title">You have 3 new Messages<a href="#">view all</a></span>--}}
{{--                        <div class="nofity-list">--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/author/author-img1.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Jhon Doe</h3>--}}
{{--                                    <span class="msg">Hello are you there?</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/author/author-img2.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>David Boos</h3>--}}
{{--                                    <span class="msg">Waiting for your Response...</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/user.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Jason Roy</h3>--}}
{{--                                    <span class="msg">Hi there, Hope you are well</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/author/author-img4.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Malika Roy</h3>--}}
{{--                                    <span class="msg">Your Product Dispatched form Warehouse...</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/author/author-img2.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Raven Jhon</h3>--}}
{{--                                    <span class="msg">Please recieve your parcel from our store</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/author/author-img1.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Angela Yo</h3>--}}
{{--                                    <span class="msg">You recieved a new message...</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="#" class="notify-item">--}}
{{--                                <div class="notify-thumb">--}}
{{--                                    <img src="{{asset('assets/images/user.jpg')}}" alt="image">--}}
{{--                                </div>--}}
{{--                                <div class="notify-text">--}}
{{--                                    <h3>Rebecca Jhon</h3>--}}
{{--                                    <span class="msg">Hey I am waiting for you...</span>--}}
{{--                                    <span>3:15 PM</span>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="settings-btn d_none_sm">--}}
{{--                    <i class="ti-more"></i>--}}
{{--                </li>--}}
                <li class="user-dropdown">
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="d_none_sm">{{Auth::user()->name}} <i class="feather ft-chevron-down"></i></span>
                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhIPEBANFREVEBAVEhAVEBAVFQ8VFhUWGBYVFRUYHSggGBolGxYVITEhJSkrLi4uFx8zODUsNygtLisBCgoKDg0OGhAQGi0gICUtLS0tLS0tNS0tKy0tKy0tKy0tLS0wLS0tLS0tLS0tLS0tLS41Ky0tLS0tLS0tLTUtLf/AABEIAL8BCAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQECAwQGBwj/xAA8EAACAQMBBQcBBQYGAwEAAAAAAQIDBBEhBRIxQVEGEyJhcYGRoQcyQrHBFCNSktHhM2KCk6LwQ7LxF//EABgBAQEBAQEAAAAAAAAAAAAAAAABAgQD/8QAIBEBAAMAAwEAAgMAAAAAAAAAAAECEQMhMRITQWFxkf/aAAwDAQACEQMRAD8A9qAAAAAAAAAAAAAACgFQUyN4CoLd8pvoC8Fm+iu+BcC3eK5AqCmSoAAAAAAAAAAAAAAAAAAAAAAAAAAAACjAFGwcT2u+0S3tN+nRj39eP3oxklTpvTSc+vks8MNoDtHI0K217aM1TlcW0ajeFTdampN9FFvOT58272tv71yhdVKipcVbwSo0umJPO9JYf4nLOOBB3lF4cIU1mKS1UJJxzhOTkl4c6ZC4+p3ULXUPmbZPbO9tPBSnKnut5gtacteLpvTi+STw85PUuyn2lUbmUaNdRpVpaQkm+7qyxrHXWEvJtp9eQMej94XKoRauDJC4CJNTL1I0IVjYhMDaTKmGMjImBeCiKgAAAAAAAAAAAAAAAAAAAAAAAAC1lWR239pK2tq9005KjRqVN1fi3YtpfIHD/ax2x7iErGg5d7OH76Uc5pwktIRa4Tks+i82jxtqO9os5lpHwrTTguOcLnxfp4t7YVtX2hdt1ajdSrNyqz44WW546aPEVy06Hs2xuz9vbxSpU4p41qNJzl5uX6cDFrY9qU14HUtrnvVOlQuMa4at9HnXGFHH5+ZtOyvt5OFpcJvOqp1IuWV0xj3xldT6IdAwV6Rj8ktxxw+aLmxuE3v062j/AIZaGs95PEoyi/NPX59vjmfSlSloc5tzZtKa8dKElrnMUyRyr+H+UP8AZp2unWg7W4qJ1YY7uTl4qsNXu5by5Rx8Y6M7+ndHz7tq0nY3UatBtKM1Om2311jLm1xXoz2Cw2iqkIVFwnCMlrnSST4+57xOuaYyXXUbg3qNY5m2uSVtq4ROU5mxFkdQqG5TkBsoqiyLL0BUAAAAAAAAAAAAAAAAAAAAAAAFrOe7erOz7xPGHQlx4cUdCyC7ZpOxus4/wJ8eGiysgeWdgbONJwn+KWr01eXhL6p/J6fQPPOxqzXx0pzl55i1HL/mOm2h2kpUJd3rOeNYR1a6LC1bOe3rrr46RvQ1KhzMO3ElJKdleqDf33TaSXN4lh+2MnQ0LyNWO/HOPNGZahirkTdRzkxdou0lO3X3JTlyhHGX6HMS7ZT1dSzuIR5OUZJP3xp7mYjfG9z1zXbmnlejl/1Ev2Qu27Whniobv8snFfRIjtv3dOrTlVhqt5Y8sp6Mu7OPdoU455N/Mm/1Onj8cnN67m0uCbs6xyFnWJ+xqm3k6m1qEnRkQNnMmLeQEhBmRGCmzOgLgURUAAAAAAAAAAAAAAAAAAABRlSjAtZB9qKHe0pW+/KMasKkZSSTaWEs/UnJEVtpNxSTazlby/DlcfIzechvjjbQ897MWcqN3XpSUVKFFarhKL3XGSfR4fxjkS1xsx4/dyVPLzJxg5zqv/NrnHv66Gx3EIXVTHF21usvjhTr/wBWTFGC4nPaXVXpwkuz1Wc8u4rTwsLNrThGL3s7ywlHh4dOWvHU7Cws+6huJ58Ovrz9DLVqwjJR4t8kvlszKLfL014k9lryHmdLZsp39WrUnmMZJRTz4Vz3eSfDVpryMlWwu05L9rtqud3FPu3HhFKWNzhl5eqeM48yav6UaNebytU5Yzz9fMmratGdKM4SzGUU01zT8ibOY1kbry7tVs+MITUIJSk6e9BZeW5qP6vkjDbUnTUabkm0vE11bbx9UdB2rptNv0az1Uk4/VI52m5aKecxcoKT4zUdFJ9W3nL6pntxy8OasZMpqzmdBYTOZs2dBYPgezldPYyJu1ZAWLJ21YElSZsRNakbMQLkVKIqAAAAAAAAAAAAAAAAAAAAoypRgWyNa5jlNfHqbLMNQDgtvuVK5oPxeKlOLzjVQkny0/GSVO9zHTjjh5aYb6cTD27o4jRr6/u6u7LXHhqYX/soL3IGjtBJxw9XiL1XHXi/LX10Ofkrjr47bGtqz29HenUzTjFNx7ypJJZTxiPvkiLibg3Vtbm1UpSk3T71wWXrph4fHmVsuzlKe5ddzGrOlKolSnOUadXx531jOJLxLg0868ES93t67UZJbOtJ0mt3clWlFvO4m3N03D8L89V5krD3iJmNiN/x5xtyjcVam/VuLTC0S7zOVz1z+p1fZnbsaahbSUUmkqcoyzGWuMeprdodoVa0KtJ7MtKe+ppTp11OUW3DDe7TS4Q5dV551th7Jo2apue5vvfnPKj4YxSklHpqlrx4C1ekyY7mM/tk7WXOd5ZWd6CfvJcSHqzTm2sY0SwsLTovXL9zUuto9/4nxlPPsmmvyb9zNQR68dchy8196StmdDYciAs1wOhsFwPR4OhsSdtSEsUTlqgJGkbMTXpGxEC5FSiKgAAAAAAAAAAAAAAAAAAAAAFrMNRGZmOaAitrWca1OdGed2cXF44rPBrzTw16HjF/CtCdS3nlT8OMYUaiim95Z6rX6dT3Ksji+0mxIV85WJxctya4x1+q8jF5x68f7QPZna8t9xWNx5cZa/heGn566+jJvaF5NwcqEoSzFvdaS9OevJfJ5pf0riyqapuGdZJLdlrn2by3u/Vk3adroyp6SSkt7ei1zeGmpceOePU8vn9w6KXz1LSu5rPeRhHjp7/X/vkcR2n2y5Skk9HTxGXXe448sZ+MGXanaGMllpLTTD+jSSy/jHwRWzLF1JKtWWIf+Om/xdMr+H8/QsRncl7/AF1DLZ28oqLl+KKcV0Wv9CSt0Zb+OkJdVJfGP6ltsj1rOxrlvGWxK2UTorCJBWSOisEaYTtjEm7VERZImrZAbtIzow0zMgLkVKIqAAAAAAAAAAAAAAAAAAAAFMlMgGWSLmzHKQGGoiFu4YnJf5mTtOOWVu7GNTXhLH3uvquZi9dhvjt8y4y82VCplvizi9vdi4N78acG9eqf0/oem3drKDxJe/J+hHXUXh6J+RzTsOyJizyF7GVJ6UIb3WTc8emdC+3tZt5lnj8nd1dnZy5JZbK23Z+VWW5BJfxTa0gvPz6Ln6ZakWmempite3NVNnOrTm0n+5pTqN8lqtH6qMvgi7Y9eo7LpUqboxjmLT388amVhuXqeb7R2BVt3N4cqUZY31rurjHfXLTGvA7K1+axDgvb6tMslkdDYHN2UjoLGZWXS2bJm3ZAWdQmLeoBKU2Zos1KczPGYGYqWKRVMC8FuSoFQAAAAAAAAAAAAAoUKNgVbLHIoVpxXPHoBTV8EU7p8XwNhIpICylBcur1Mpi4cPgyp51ApOKaw0muaa0OQ7RX9lbVIUql1b05zWVSqVIxxHXxNvSMdMLexl8OhIduu0n7BaVK8Y79bdl3NPjvSx96SX4I8X8c0fNn7S7qc61aUpVpSzUm3l1G+b+ixywkhNIt61W018e9W1W3qTSdxRUcpb0Zxmm8ZxvRzGOnNvmuJ1cLeEI7sElFcv1fVnyTUu5xk9yUotbyzF4eHo1la4Z7Z9kt9eRtoxuZ1KlB/wCG5Q8VstcR328zhw0x4eCbSFOOK+F7zb13NdamDZqi6lWLXCSjLzzThJL4kzcjDLwa/Z2PeQlW0SqzlOPVx4Q/4qJ6PJo3mx7aUt10afqlute6Im92G6K36cnKPOL+8vjidDbpzrVm14YSjH33U37ar4JCUU1hxWPPImIVxtpXJe3rkhPZ9F5fdQXoks/Qtjs6k/upx88v+5n5XV9GubUKxEVacqbw9V1RfSuTKpuNQyRmRdO4NmnVA31IuTNaEzLGQGUqWplUBUAAAAAAAAowyjAo2V3MrTj8lEXx8ijAl/8APzRTcM8o8/kskvqXUWYa1Xv5F8JNiHHHUpBYk111QGTyEHjiWVXjDKyWVkiuPls+pdXlS6qZ7iEpUaNNrRxhJqU8PrNSafTHkea/ad2Tp2NxRrW0VGlcOpGVPPhhUjuyW6uKTTenBbvLKR7jJPhy/I8m+2/aOaljbLGU6lafVZ8EPZ4qfCNwjzHYGx3cXX7NnTvfFJL8KeZadT6a2RZwp0o04pKKikl5YPnTs5tGNntOlWlju5NRqN40jU0cn0w8S9In0pZvQDDcUPC466+HzSlp9Mm3bLXhwWnkW1uKMlLQg1Nn08SqZ4ueX+X6GepDjgo/4uafyuZli/G11igNS4+7FLjOaj7Ycn9IsujHXHLmZa8cSguSjUfvmKX0bLZS3Yzn0i2vXGi+SjUks70ljWWFpw3efvhkPtaShNJc1l+pN1Zwo0d6fCKXq29El5vgcntmpKSdWWjTWnTLxj8hPcJDfoXRv0a5yltdErbXJ5tOlo1TapzIW3rElRqAb8WXowU5GZAXgoioAAAAABRlkmXMsaAvprQrgtjFrhqunP5L1h/qUVRZUjp9UXscyDEnlJlay/F0/LmILivPPyXooxXD8LfTUugy3d0lH/uORZSlpH0CLJ6Sa6rQ8C+0uu6u1q+eFJUqcfRU4yf/ACnM98v3hRn0az6M+d+0NVzvb2b4/tl1Fekasor6RRuBA7dt8xU+n5HvH2Wbc/a7GjOUs1ILuquurlDTL82t2X+o8WuYZg0dJ9hu03Tua1o34akFUiukoPdl8qUf5Sz6Q91qrg/Mu8w9UUXAyKLmY4PxfCMjZjp/eXqBkr6ya5qMfq5f0K1qKkowfDeUmuu601/y3TBQe9XrPlGNCP8Aq/eSf0lEz1pre3ekVvPlGPF59cfn0IInbD3qkFpuQXeNfxSz+7X8y3v9KIfbdo3byjnEpOL+GpfoTdOG/UlNrRtNLy4QT9sv1kYLujvz3V79F1NwjzmhWabi9Gm010a4ol7O4IPbFxF3Vdx+73skvPDw37tN+5tWVY8mnXWdYmLaocxZVSdtJgTdKRsRZpUJG3BgZkVLUXAAAAAAFMFN0uABDzAANgABzyAAKNcyyNJLqZABjq0VJbrzhnJV/s12dOc6klc705znL99o5Tk5PTHVs7EF2Rxf/wCYbN6XP+9/Yv2N9muzrWvG6oq5VWOcN1m1qtcrB2IGyLFTQ7teZeCaMfdLzEaKTzqZAXRio28YubWczmpS15qMY/lFCdCLTTzhvL/zeT8tMehlBBhjbxWeOr1eTGrGGJLxeLi86/JtAuyOYuOwllOcqku/3pPLxUwuGOGC+n2Is48O/wD9xf0OkBBC0uzNvHh3v8/9jbp7JpR4b/8AN/Y3wBghaxXDPyZFTXmXgCmCoAAAAf/Z" alt="" class="img-fluid">
                        </button>
                        <div class="dropdown-menu dropdown_user" aria-labelledby="dropdownMenuButton" >
                            <div class="dropdown-header d-flex flex-column align-items-center">
                                <div class="user_img mb-3">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhIPEBANFREVEBAVEhAVEBAVFQ8VFhUWGBYVFRUYHSggGBolGxYVITEhJSkrLi4uFx8zODUsNygtLisBCgoKDg0OGhAQGi0gICUtLS0tLS0tNS0tKy0tKy0tKy0tLS0wLS0tLS0tLS0tLS0tLS41Ky0tLS0tLS0tLTUtLf/AABEIAL8BCAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQECAwQGBwj/xAA8EAACAQMBBQcBBQYGAwEAAAAAAQIDBBEhBRIxQVEGEyJhcYGRoQcyQrHBFCNSktHhM2KCk6LwQ7LxF//EABgBAQEBAQEAAAAAAAAAAAAAAAABAgQD/8QAIBEBAAMAAwEAAgMAAAAAAAAAAAECEQMhMRITQWFxkf/aAAwDAQACEQMRAD8A9qAAAAAAAAAAAAAACgFQUyN4CoLd8pvoC8Fm+iu+BcC3eK5AqCmSoAAAAAAAAAAAAAAAAAAAAAAAAAAAACjAFGwcT2u+0S3tN+nRj39eP3oxklTpvTSc+vks8MNoDtHI0K217aM1TlcW0ajeFTdampN9FFvOT58272tv71yhdVKipcVbwSo0umJPO9JYf4nLOOBB3lF4cIU1mKS1UJJxzhOTkl4c6ZC4+p3ULXUPmbZPbO9tPBSnKnut5gtacteLpvTi+STw85PUuyn2lUbmUaNdRpVpaQkm+7qyxrHXWEvJtp9eQMej94XKoRauDJC4CJNTL1I0IVjYhMDaTKmGMjImBeCiKgAAAAAAAAAAAAAAAAAAAAAAAAC1lWR239pK2tq9005KjRqVN1fi3YtpfIHD/ax2x7iErGg5d7OH76Uc5pwktIRa4Tks+i82jxtqO9os5lpHwrTTguOcLnxfp4t7YVtX2hdt1ajdSrNyqz44WW546aPEVy06Hs2xuz9vbxSpU4p41qNJzl5uX6cDFrY9qU14HUtrnvVOlQuMa4at9HnXGFHH5+ZtOyvt5OFpcJvOqp1IuWV0xj3xldT6IdAwV6Rj8ktxxw+aLmxuE3v062j/AIZaGs95PEoyi/NPX59vjmfSlSloc5tzZtKa8dKElrnMUyRyr+H+UP8AZp2unWg7W4qJ1YY7uTl4qsNXu5by5Rx8Y6M7+ndHz7tq0nY3UatBtKM1Om2311jLm1xXoz2Cw2iqkIVFwnCMlrnSST4+57xOuaYyXXUbg3qNY5m2uSVtq4ROU5mxFkdQqG5TkBsoqiyLL0BUAAAAAAAAAAAAAAAAAAAAAAAFrOe7erOz7xPGHQlx4cUdCyC7ZpOxus4/wJ8eGiysgeWdgbONJwn+KWr01eXhL6p/J6fQPPOxqzXx0pzl55i1HL/mOm2h2kpUJd3rOeNYR1a6LC1bOe3rrr46RvQ1KhzMO3ElJKdleqDf33TaSXN4lh+2MnQ0LyNWO/HOPNGZahirkTdRzkxdou0lO3X3JTlyhHGX6HMS7ZT1dSzuIR5OUZJP3xp7mYjfG9z1zXbmnlejl/1Ev2Qu27Whniobv8snFfRIjtv3dOrTlVhqt5Y8sp6Mu7OPdoU455N/Mm/1Onj8cnN67m0uCbs6xyFnWJ+xqm3k6m1qEnRkQNnMmLeQEhBmRGCmzOgLgURUAAAAAAAAAAAAAAAAAAABRlSjAtZB9qKHe0pW+/KMasKkZSSTaWEs/UnJEVtpNxSTazlby/DlcfIzechvjjbQ897MWcqN3XpSUVKFFarhKL3XGSfR4fxjkS1xsx4/dyVPLzJxg5zqv/NrnHv66Gx3EIXVTHF21usvjhTr/wBWTFGC4nPaXVXpwkuz1Wc8u4rTwsLNrThGL3s7ywlHh4dOWvHU7Cws+6huJ58Ovrz9DLVqwjJR4t8kvlszKLfL014k9lryHmdLZsp39WrUnmMZJRTz4Vz3eSfDVpryMlWwu05L9rtqud3FPu3HhFKWNzhl5eqeM48yav6UaNebytU5Yzz9fMmratGdKM4SzGUU01zT8ibOY1kbry7tVs+MITUIJSk6e9BZeW5qP6vkjDbUnTUabkm0vE11bbx9UdB2rptNv0az1Uk4/VI52m5aKecxcoKT4zUdFJ9W3nL6pntxy8OasZMpqzmdBYTOZs2dBYPgezldPYyJu1ZAWLJ21YElSZsRNakbMQLkVKIqAAAAAAAAAAAAAAAAAAAAoypRgWyNa5jlNfHqbLMNQDgtvuVK5oPxeKlOLzjVQkny0/GSVO9zHTjjh5aYb6cTD27o4jRr6/u6u7LXHhqYX/soL3IGjtBJxw9XiL1XHXi/LX10Ofkrjr47bGtqz29HenUzTjFNx7ypJJZTxiPvkiLibg3Vtbm1UpSk3T71wWXrph4fHmVsuzlKe5ddzGrOlKolSnOUadXx531jOJLxLg0868ES93t67UZJbOtJ0mt3clWlFvO4m3N03D8L89V5krD3iJmNiN/x5xtyjcVam/VuLTC0S7zOVz1z+p1fZnbsaahbSUUmkqcoyzGWuMeprdodoVa0KtJ7MtKe+ppTp11OUW3DDe7TS4Q5dV551th7Jo2apue5vvfnPKj4YxSklHpqlrx4C1ekyY7mM/tk7WXOd5ZWd6CfvJcSHqzTm2sY0SwsLTovXL9zUuto9/4nxlPPsmmvyb9zNQR68dchy8196StmdDYciAs1wOhsFwPR4OhsSdtSEsUTlqgJGkbMTXpGxEC5FSiKgAAAAAAAAAAAAAAAAAAAAAFrMNRGZmOaAitrWca1OdGed2cXF44rPBrzTw16HjF/CtCdS3nlT8OMYUaiim95Z6rX6dT3Ksji+0mxIV85WJxctya4x1+q8jF5x68f7QPZna8t9xWNx5cZa/heGn566+jJvaF5NwcqEoSzFvdaS9OevJfJ5pf0riyqapuGdZJLdlrn2by3u/Vk3adroyp6SSkt7ei1zeGmpceOePU8vn9w6KXz1LSu5rPeRhHjp7/X/vkcR2n2y5Skk9HTxGXXe448sZ+MGXanaGMllpLTTD+jSSy/jHwRWzLF1JKtWWIf+Om/xdMr+H8/QsRncl7/AF1DLZ28oqLl+KKcV0Wv9CSt0Zb+OkJdVJfGP6ltsj1rOxrlvGWxK2UTorCJBWSOisEaYTtjEm7VERZImrZAbtIzow0zMgLkVKIqAAAAAAAAAAAAAAAAAAAAFMlMgGWSLmzHKQGGoiFu4YnJf5mTtOOWVu7GNTXhLH3uvquZi9dhvjt8y4y82VCplvizi9vdi4N78acG9eqf0/oem3drKDxJe/J+hHXUXh6J+RzTsOyJizyF7GVJ6UIb3WTc8emdC+3tZt5lnj8nd1dnZy5JZbK23Z+VWW5BJfxTa0gvPz6Ln6ZakWmempite3NVNnOrTm0n+5pTqN8lqtH6qMvgi7Y9eo7LpUqboxjmLT388amVhuXqeb7R2BVt3N4cqUZY31rurjHfXLTGvA7K1+axDgvb6tMslkdDYHN2UjoLGZWXS2bJm3ZAWdQmLeoBKU2Zos1KczPGYGYqWKRVMC8FuSoFQAAAAAAAAAAAAAoUKNgVbLHIoVpxXPHoBTV8EU7p8XwNhIpICylBcur1Mpi4cPgyp51ApOKaw0muaa0OQ7RX9lbVIUql1b05zWVSqVIxxHXxNvSMdMLexl8OhIduu0n7BaVK8Y79bdl3NPjvSx96SX4I8X8c0fNn7S7qc61aUpVpSzUm3l1G+b+ixywkhNIt61W018e9W1W3qTSdxRUcpb0Zxmm8ZxvRzGOnNvmuJ1cLeEI7sElFcv1fVnyTUu5xk9yUotbyzF4eHo1la4Z7Z9kt9eRtoxuZ1KlB/wCG5Q8VstcR328zhw0x4eCbSFOOK+F7zb13NdamDZqi6lWLXCSjLzzThJL4kzcjDLwa/Z2PeQlW0SqzlOPVx4Q/4qJ6PJo3mx7aUt10afqlute6Im92G6K36cnKPOL+8vjidDbpzrVm14YSjH33U37ar4JCUU1hxWPPImIVxtpXJe3rkhPZ9F5fdQXoks/Qtjs6k/upx88v+5n5XV9GubUKxEVacqbw9V1RfSuTKpuNQyRmRdO4NmnVA31IuTNaEzLGQGUqWplUBUAAAAAAAAowyjAo2V3MrTj8lEXx8ijAl/8APzRTcM8o8/kskvqXUWYa1Xv5F8JNiHHHUpBYk111QGTyEHjiWVXjDKyWVkiuPls+pdXlS6qZ7iEpUaNNrRxhJqU8PrNSafTHkea/ad2Tp2NxRrW0VGlcOpGVPPhhUjuyW6uKTTenBbvLKR7jJPhy/I8m+2/aOaljbLGU6lafVZ8EPZ4qfCNwjzHYGx3cXX7NnTvfFJL8KeZadT6a2RZwp0o04pKKikl5YPnTs5tGNntOlWlju5NRqN40jU0cn0w8S9In0pZvQDDcUPC466+HzSlp9Mm3bLXhwWnkW1uKMlLQg1Nn08SqZ4ueX+X6GepDjgo/4uafyuZli/G11igNS4+7FLjOaj7Ycn9IsujHXHLmZa8cSguSjUfvmKX0bLZS3Yzn0i2vXGi+SjUks70ljWWFpw3efvhkPtaShNJc1l+pN1Zwo0d6fCKXq29El5vgcntmpKSdWWjTWnTLxj8hPcJDfoXRv0a5yltdErbXJ5tOlo1TapzIW3rElRqAb8WXowU5GZAXgoioAAAAABRlkmXMsaAvprQrgtjFrhqunP5L1h/qUVRZUjp9UXscyDEnlJlay/F0/LmILivPPyXooxXD8LfTUugy3d0lH/uORZSlpH0CLJ6Sa6rQ8C+0uu6u1q+eFJUqcfRU4yf/ACnM98v3hRn0az6M+d+0NVzvb2b4/tl1Fekasor6RRuBA7dt8xU+n5HvH2Wbc/a7GjOUs1ILuquurlDTL82t2X+o8WuYZg0dJ9hu03Tua1o34akFUiukoPdl8qUf5Sz6Q91qrg/Mu8w9UUXAyKLmY4PxfCMjZjp/eXqBkr6ya5qMfq5f0K1qKkowfDeUmuu601/y3TBQe9XrPlGNCP8Aq/eSf0lEz1pre3ekVvPlGPF59cfn0IInbD3qkFpuQXeNfxSz+7X8y3v9KIfbdo3byjnEpOL+GpfoTdOG/UlNrRtNLy4QT9sv1kYLujvz3V79F1NwjzmhWabi9Gm010a4ol7O4IPbFxF3Vdx+73skvPDw37tN+5tWVY8mnXWdYmLaocxZVSdtJgTdKRsRZpUJG3BgZkVLUXAAAAAAFMFN0uABDzAANgABzyAAKNcyyNJLqZABjq0VJbrzhnJV/s12dOc6klc705znL99o5Tk5PTHVs7EF2Rxf/wCYbN6XP+9/Yv2N9muzrWvG6oq5VWOcN1m1qtcrB2IGyLFTQ7teZeCaMfdLzEaKTzqZAXRio28YubWczmpS15qMY/lFCdCLTTzhvL/zeT8tMehlBBhjbxWeOr1eTGrGGJLxeLi86/JtAuyOYuOwllOcqku/3pPLxUwuGOGC+n2Is48O/wD9xf0OkBBC0uzNvHh3v8/9jbp7JpR4b/8AN/Y3wBghaxXDPyZFTXmXgCmCoAAAAf/Z" alt="" class="img-fluid">
                                </div>
                                <div class="user_bio text-center">
                                    <p class="name font-weight-bold mb-0">{{Auth::user()->name}}</p>
                                    <p class="email text-muted mb-3">{{Auth::user()->email}}</p>
                                </div>
                            </div>
{{--                            <a class="dropdown-item" href="#"><i class="ti-user"></i> Profile</a>--}}
{{--                            <a class="dropdown-item" href="#"><i class="ti-settings"></i> Account Settings</a>--}}
                            <span role="separator" class="divider"></span>
                            <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="ti-power-off"></i>Logout</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!--==================================*
                 End Notification Section
        *====================================-->

    </div>
</div>
